<?php

	use Monolog\Logger;

	const VERSION   = "1.0.0-dev";
	const LOG_LEVEL = Logger::INFO;

	const HOME_DIR = "";
	const LOG_FILE = HOME_DIR . "MampfBot.log";

	const MAMPF_ENDPOINT = "";
