<?php

	namespace MampfBot\Traits;

	trait Singleton {
		private static $_instance;

		private function __construct() {
		}

		/**
		 * @return $this
		 */
		public static function getInstance() {
			return self::$_instance ?: (self::$_instance = new self);
		}
	}

