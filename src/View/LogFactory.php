<?php

	namespace MampfBot\View;

	use Monolog\Formatter\LineFormatter;
	use Monolog\Handler\RotatingFileHandler;
	use Monolog\Handler\StreamHandler;
	use Monolog\Logger;
	use Monolog\Processor\IntrospectionProcessor;

	/**
	 * Factory class to create a Logger instance
	 *
	 * @package MampfBot\View
	 */
	class LogFactory {

		/**
		 * @var Logger $logger
		 */
		private static $logger;

		public static function get(): Logger {
			return self::$logger ?: (self::$logger = self::createLogger());
		}

		private static function createLogger(): Logger {
			$formatter = new LineFormatter("[%datetime%] %channel%.%level_name%: %message% %context% %extra%\n");
			$formatter->includeStacktraces(true);
			$fileLogger = new RotatingFileHandler(LOG_FILE, 60, LOG_LEVEL);
			$fileLogger->setFormatter($formatter);
			$cliLogger = new StreamHandler('php://stdout', LOG_LEVEL);

			return new Logger("MampfBotLogger", [$fileLogger, $cliLogger], [new IntrospectionProcessor(Logger::WARNING)]);
		}
	}
