<?php

	namespace MampfBot\Model\Mampf;

	class Mampf {
		/**
		 * @var int Mampf id
		 * @internal
		 */
		public $id;
		/**
		 * @var \DateTime Time when this Mampf takes place
		 */
		public $time;
		/**
		 * @var string The main couse
		 */
		public $mainCourse;
		/**
		 * @var string Optional supplement
		 */
		public $garnish = "";
		/**
		 * @var string Optional starter
		 */
		public $starter = "";
		/**
		 * @var string Optional dessert
		 */
		public $dessert = "";
		/**
		 * @var bool True if a vegetarian main course is possible
		 */
		public $veggie = false;
		/**
		 * @var string Optional remark for this Mampf
		 */
		public $comment = "";
		/**
		 * @var \DateTime Deadline for entering Mampf
		 */
		public $registrationDeadline;
		/**
		 * @var string Description where the Mampf takes place
		 */
		public $place;
		/**
		 * @var int Maximum number of attendees
		 */
		public $guestLimit;
		/**
		 * @var float Estimated Mampf cost
		 */
		public $cost;
	}
