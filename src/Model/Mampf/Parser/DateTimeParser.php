<?php

	namespace MampfBot\Model\Mampf\Parser;

	use MampfBot\Model\Mampf\Mampf;

	class DateTimeParser implements IMampfParser {

		/**
		 * Parses a DOMElement and manipulates a Mampf object accordingly
		 *
		 * @param \DOMElement $node  Node to get information from
		 * @param Mampf       $mampf Mampf object to store information to
		 *
		 * @return bool Returns true on success. If no suitable information was found in $node, returns false
		 */
		public function parse(\DOMElement $node, Mampf $mampf): bool {
			/**
			 * @var \DOMElement $h2s
			 */
			foreach($node->getElementsByTagName("h2") as $h2) {
				$matches = [];
				if(preg_match("/(\d{4}-\d{2}-\d{2}).*(\d{2}:\d{2}:\d{2})/s", $h2->nodeValue, $matches) &&
				   count($matches) > 2 &&
				   ($datetime = \DateTime::createFromFormat("Y-m-dH:i:s", $matches[1] . $matches[2])) !== false) {
					$mampf->time = $datetime;

					return true;
				}
			}

			return false;
		}
	}
