<?php

	namespace MampfBot\Model\Mampf\Parser;

	use MampfBot\Model\Mampf\Mampf;

	interface IMampfParser {
		/**
		 * Parses a DOMElement and manipulates a Mampf object accordingly
		 *
		 * @param \DOMElement $node  Node to get information from
		 * @param Mampf       $mampf Mampf object to store information to
		 *
		 * @return bool Returns true on success. If no suitable information was found in $node, returns false
		 */
		public function parse(\DOMElement $node, Mampf $mampf): bool;
	}
