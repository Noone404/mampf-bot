<?php

	namespace MampfBot\Model\Mampf;

	use DOMDocument;
	use DOMXPath;
	use MampfBot\Model\Mampf\Parser\DateTimeParser;
	use MampfBot\Model\Mampf\Parser\IMampfParser;
	use MampfBot\Traits\Singleton;

	class MampfFactory {
		use Singleton;

		private $provider = [
			DateTimeParser::class => false
		];

		/**
		 * @param \DateTime|null $start
		 * @param \DateTime|null $end
		 *
		 * @return array|Mampf[]
		 */
		public function fetch(\DateTime $start = null, \DateTime $end = null): array {
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, MAMPF_ENDPOINT . "/home.php");
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

			if($start || $end) {
				curl_setopt($curl, CURLOPT_POST, true);
				$startstr = $start ? $start->format("Y-m-d") : "";
				$endstr   = $end ? $end->format("Y-m-d") : "";
				curl_setopt($curl, CURLOPT_POSTFIELDS, "startdate=$startstr&enddate=$endstr");
			}

			$answer = curl_exec($curl);
			if(!$answer)
				return [];

			$mampfs = [];

			$dom = new DOMDocument();
			$dom->loadHTML($answer);
			foreach((new DomXPath($dom))->query("//*[contains(@class, 'mampf')]") as $node) {
				$mampf = new Mampf();

				/**
				 * @var IMampfParser $parser
				 */
				foreach($this->provider as $parser => $mandatory)
					if(!$parser->parse($node, $mampf) && $mandatory)
						continue 2;

				$mampfs[] = $mampf;
			}

			return $mampfs;
		}
	}
